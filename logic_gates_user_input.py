# -*- coding: utf-8 -*-
"""
based on/ homework for
http://interactivepython.org/runestone/static/pythonds/Introduction/ObjectOrientedProgramminginPythonDefiningClasses.html

doesn't seem quite right that e.g.

    self.pinA.getFrom().getOutput()

Binarygate needs to kow it is connected to something... logic flows wrong way???

but hey... it is what the instructors wrote.


Created on Sun Sep 20 15:52:19 2015

@author: laurence
"""


class LogicGate(object):
    def __init__(self, n):
        self.name = n
        self.output = None

    def getName(self):
        return self.name

    def getOutput(self):
        self.output = self.performGateLogic()
        return self.output


class BinaryGate(LogicGate):
    def __init__(self, n):
        LogicGate.__init__(self, n)

        self.pinA = None
        self.pinB = None

    def getPinA(self):
        if self.pinA is None:
            return int(input("Enter Pin A input for gate "+self.getName() + "-->"))
        else:
            return self.pinA.getFrom().getOutput()

    def getPinB(self):
        if self.pinB is None:
            return int(input("Enter Pin B input for gate " +self.getName() + "-->"))
        else:
            return self.pinB.getFrom().getOutput()

    def setNextPin(self, source):
        if self.pinA is None:
            self.pinA = source
        else:
            if self.pinB is None:
                self.pinB = source
            else:
                print("Cannot Connect: NO EMPTY PINS on this gate")


class AndGate(BinaryGate):

    def __init__(self, n):
        BinaryGate.__init__(self, n)

    def performGateLogic(self):

        a = self.getPinA()
        b = self.getPinB()
        if a == 1 and b == 1:
            return 1
        else:
            return 0


class OrGate(BinaryGate):
    def __init__(self, n):
        BinaryGate.__init__(self, n)

    def performGateLogic(self):

        a = self.getPinA()
        b = self.getPinB()
        if a == 1 or b == 1:
            return 1
        else:
            return 0


class UnaryGate(LogicGate):
    def __init__(self, n):
        LogicGate.__init__(self, n)

        self.pin = None

    def getPin(self):
        if self.pin is None:
            return int(input("Enter Pin input for gate " +
                             self.getName() + "-->"))
        else:
            return self.pin.getFrom().getOutput()

    def setNextPin(self, source):
        if self.pin is None:
            self.pin = source
        else:
            print("Cannot Connect: NO EMPTY PINS on this gate")


class NotGate(UnaryGate):

    def __init__(self, n):
        UnaryGate.__init__(self, n)

    def performGateLogic(self):
        if self.getPin():
            return 0
        else:
            return 1


class NandGate(BinaryGate):
    def __init__(self, n):
        # could also inherit fron AndGate and invert ourputs see NorGate
        self.andy = AndGate(n)
        self.notty = NotGate('Notty')
        self.connect = Connector(self.andy, self.notty)

    def performGateLogic(self):
        return self.notty.getOutput()


class NorGate(OrGate):
    # is this easier/cleaner than encapsulating a connector like nand?
    def performGateLogic(self):
        if super(NorGate, self).performGateLogic() == 1:
            return 0
        else:
            return 1


class Connector:
    def __init__(self, fgate, tgate):
        self.fromgate = fgate
        self.togate = tgate

        tgate.setNextPin(self)

    def getFrom(self):
        return self.fromgate

    def getTo(self):
        return self.togate


def extension():
    """
    show
        NOT (( A and B) or (C and D))
    has same truth table as
        NOT( A and B ) and NOT (C and D)
    """
    g_a_a_b = AndGate("A and B")
    g_c_a_d = AndGate("C and D")
    g_ab_nor_cd = NorGate("A&B nor C&D")
    c1 = Connector(g_a_a_b, g_ab_nor_cd)
    c2 = Connector(g_c_a_d, g_ab_nor_cd)
    print(g_ab_nor_cd.getOutput())
    
    g_a_na_b = NandGate("A and B")
    g_c_na_d = NandGate("C and D")
    g_ab_and_cd = AndGate("and(nand(A, B), nand(C,D))")
    c1 = Connector(g_a_na_b, g_ab_and_cd)
    c2 = Connector(g_c_na_d, g_ab_and_cd)
    print(g_ab_and_cd.getOutput())



def their_main():
    g1 = AndGate("G1")
    g2 = AndGate("G2")
    g3 = OrGate("G3")
    g4 = NotGate("G4")
    c1 = Connector(g1, g3)
    c2 = Connector(g2, g3)
    c3 = Connector(g3, g4)
    print(g4.getOutput())
