# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 20:15:49 2015

@author: laurence
"""
from random import choice, randint

TARGET = 'methinks it is like a weasel'
# a-z and space
CHARS = [chr(c) for c in range(97, 123) + [32]]


def random_char():
    return choice(CHARS)


def random_chars(n_chars):
    for i in range(n_chars):
        yield random_char()


def scorer(guess, target):
    for i, char in enumerate(target):
        if guess[i] == char:
            yield 1


def get_score(guess, target):
    return sum(scorer(guess, target))


def single_select_weasel(target):
    n_guesses = 0
    best_score = 0
    best_guess = ''
    while n_guesses < 1e8:
        n_guesses += 1
        # guess whole new string at random
        guess = ''.join(random_chars(len(target)))
        score = get_score(guess, target)
        if score > best_score:
            best_score = score
            best_guess = guess

        if n_guesses % 1e5 == 0:
            print ('{} iterations, best score {}/{} best guess {}'.format(
                n_guesses, best_score, len(target), best_guess
                ))
        if score == len(target):
            print('got it, generated {}, score: {}/{}'.format(
                guess, score, len(target)
                ))
            break


def evolve_weasel(target):
    n_guesses = 1
    best_guess = ''.join(random_chars(len(target)))
    best_score = sum(scorer(best_guess, target))
    while n_guesses < 1e8:
        n_guesses += 1
        # select a letter to mutate
        to_change = randint(0, len(target))
        # mutate
        guess = best_guess[:to_change] + random_char() + best_guess[to_change + 1:]
        score = get_score(guess, target)
        if score > best_score:
            best_score = score
            best_guess = guess
        if n_guesses % 1e2 == 0:
            print ('{} iterations, best score {}/{} best guess {}'.format(
                n_guesses, best_score, len(target), best_guess
                ))
        if score == len(target):
            print('got it, generated {}, score: {}/{}, {} generations'.format(
                guess, score, len(target), n_guesses
                ))
            break
    
if __name__ == '__main__':
    # single_select_weasel(TARGET)
    evolve_weasel(TARGET)
