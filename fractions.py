# -*- coding: utf-8 -*-
"""
Fraction Class after the:

data structures and algorithms in python online course
http://interactivepython.org/runestone/static/pythonds/Introduction/ObjectOrientedProgramminginPythonDefiningClasses.html
Created on Sat Sep 19 21:28:49 2015

@author: laurence
"""
from __future__ import division, print_function
import doctest


def _on_common_denom(left, right):
    """
    Return 2 `Fraction`s  (`left` and `right`) that have been put
    on a common denominator
    """
    common = left.denominator * right.denominator
    new_left = Fraction(left.numerator * right.denominator, common)
    new_right = Fraction(right.numerator * left.denominator, common)
    return new_left, new_right


def _great_common_divisor(m, n):
    """
    greatest common divisor of 2 ints `m` and `n`"
    Euclid’s Algorithm
    """
    while m % n != 0:
        oldm = m
        oldn = n
        m = oldn
        n = oldm % oldn
    return n


def _invert(frac_or_int):
    """
    utility to invert a Fraction of return 1/`int`
    function not method because has to deal with `ints` as well as
    `Fractions`
    """
    sign = 1
    if frac_or_int.numerator < 0:
        sign = -1
    inverted = sign * Fraction(frac_or_int.denominator,
                               abs(frac_or_int.numerator))
    return inverted


class Fraction(object):
    """
    for when floating point just will not do

    supports:

    * addition;
    * subtraction;
    * multiplcation and division by `Fraction` and `int`;
    * equality and ineqaulity binary comparisons

    Parameters
    ----------
    numerator: int,
        top part of the `Fraction`
    denominator: int,
        bottom part of the `Fraction` must be > 0.

    Examples
    --------
    >>> myfrac = Fraction(1, 2) + Fraction(1, 4)
    >>> myfrac
    Fraction(6, 8)
    >>> myfrac.simplify()
    Fraction(3, 4)

    >>> Fraction(3, 4) / 3 > Fraction(1, 6)
    True
    """
    def __init__(self, numerator, denominator):
        if denominator < 1:
            raise ValueError('denominator must be a least 1')
        self.denominator = denominator
        self.numerator = numerator

    def __repr__(self):
        """representation string should be pasteable into consol to recreate"""
        str_ = 'Fraction({0}, {1})'.format(self.numerator, self.denominator)
        return str_

    def __str__(self):
        """pretty print with a slash"""
        str_ = '{0}/{1}'.format(self.numerator, self.denominator)
        return str_

    def __eq__(self, other):
        """equality hook"""
        # get on common denominator
        self_trans, other_trans = _on_common_denom(self, other)
        nums_eq = self_trans.numerator == other_trans.numerator
        return nums_eq

    def __ne__(self, other):
        """!=  hook"""
        return not self == other

    def __gt__(self, other):
        """greater than >  hook"""
        self_trans, other_trans = _on_common_denom(self, other)
        return self_trans.numerator > other_trans.numerator

    def __ge__(self, other):
        """greater than or equal to >=  hook"""
        return self.__eq__(other) or self.__gt__(other)

    def __lt__(self, other):
        """less than <  hook"""
        return not self.__ge__(other)

    def __le__(self, other):
        """less than or equal to <=  hook"""
        return self.__eq__(other) or self.__lt__(other)

    def __add__(self, other):
        """hook for + to add 2 Fractions"""
        self_trans, other_trans = _on_common_denom(self, other)
        summed_num = self_trans.numerator + other_trans.numerator
        return Fraction(summed_num, self_trans.denominator)

    def __mul__(self, other):
        """
        hook for * to multiply 2 Fractions or 1 Fraction by an int
        works for `int`s
        because PEP 3141 defines numerator and denominator on `ints`
        """
        new_num = self.numerator * other.numerator
        new_den = self.denominator * other.denominator
        return Fraction(new_num, new_den)

    def __rmul__(self, other):
        """hook for * on right, see `__mul__`"""
        return self * other

    def __truediv__(self, other):
        """
        hook for / to divide 2 Fractions or 1 Fraction by an int
        works for `int`s
        because PEP 3141 defines numerator and denominator on `ints`
        """
        inverted = _invert(other)
        return self * inverted

    def __div__(self, other):
        """
        hook for / for p2k not using __future__.division
        """
        return self.__truediv__(other)

    def __rdiv__(self, other):
        """
        hook for `int`/ `Fraction` etc see issue #1
        """
        inverted = _invert(self)
        return inverted * other

    def __sub__(self, other):
        """hook for - to take on Fraction from another"""
        neg_other = Fraction(-1 * other.numerator, other.denominator)
        return self + neg_other

    def invert(self):
        """invert a fraction: swap numerator and denominator"""
        inverted = _invert(self)
        return inverted

    def simplify(self):
        """
        return the Fraction removing common devisor
        of `self.denominator` and `self.numerator`
        """
        common_divisor = _great_common_divisor(self.numerator, self.denominator)
        return Fraction(
            self.numerator//common_divisor,
            self.denominator//common_divisor
            )


def test_string():
    """do we get the correct string from our fraction?"""
    frac = Fraction(1, 2)
    expected = '1/2'
    got = str(frac)
    assert got == expected, 'str(Fraction) did not get what we wanted'


def test_bad_denom_raises():
    """do we complain on init with a zero or negative denominator"""
    try:
        Fraction(1, 0)
    except ValueError as verr:
        if 'denom' in str(verr):
            pass
    try:
        Fraction(1, -12)
    except ValueError as verr:
        if 'denom' in str(verr):
            pass


# Tests


def test_repr():
    """
    kinda evil test that we can excecute what repr returns
    needs equality to work too
    """
    frac = Fraction(1, 2)
    got = eval(repr(frac))
    assert got == frac, 'repr(Fraction) did not get what we wanted'


def test_equality():
    """
    test the == hook with
    """
    frac = Fraction(1, 2)
    eq_same_denom = Fraction(1, 2)
    eq_diff_denom = Fraction(8, 16)
    neq_same_denom = Fraction(3, 2)
    neq_diff_denom = Fraction(1, 3)
    assert frac == eq_same_denom, '== failed for same denom fracs'
    assert frac == eq_diff_denom, '== failed for different denom fracs'
    assert not frac == neq_same_denom, '== should not pass for same denom fracs'
    assert not frac == neq_diff_denom, '== should not pass for different denom fracs'


def test_not_equality():
    """
    test the == hook with
    """
    frac = Fraction(1, 2)
    eq_same_denom = Fraction(1, 2)
    neq_diff_denom = Fraction(1, 3)
    assert not frac != eq_same_denom, '!= failed for same denom fracs'
    assert frac != neq_diff_denom, '!= should  pass for different denom fracs'


def test_inequality():
    """
    test the >, <, >=, <= hooks with
    """
    assert Fraction(6, 8) > Fraction(5, 7), '> test failed'
    assert Fraction(2, 9) < Fraction(3, 11), '< test failed'
    assert Fraction(3, 4) >= Fraction(6, 8), '>= test failed when =='
    assert Fraction(6, 5) >= Fraction(3, 4), '>= test failed when >'
    assert Fraction(6, 8) <= Fraction(3, 4), '<= test failed when =='
    assert Fraction(3, 4) <= Fraction(6, 5), '<= test failed when <'


def test_simplify():
    """test simplification of numerator and denominator with common divisor"""
    not_simple = Fraction(16, 24)
    expected = Fraction(2, 3)
    assert not_simple.simplify() == expected, 'simplifying failed'


def test_add():
    """test addition by + of 2 Fractions"""
    half = Fraction(1, 2)
    two3rds = Fraction(2, 3)
    seven6ths = Fraction(7, 6)
    assert half + two3rds == seven6ths, 'addition borked'


def test_subtraction():
    """test addition by + of 2 Fractions"""
    half = Fraction(1, 2)
    two3rds = Fraction(2, 3)
    seven6ths = Fraction(7, 6)
    assert seven6ths - two3rds == half, 'subtracton borked'


def test_multiplication():
    """test multiplication by * of 2 Fractions or 1 frac and 1 int"""
    half = Fraction(1, 2)
    two3rds = Fraction(2, 3)
    assert 2 * half == half * 2 == Fraction(1, 1), 'multiply by int borked'
    assert two3rds * half == Fraction(1, 3), 'multiply by Fraction borked'


def test_division():
    """test divisio by / of 2 Fractions"""
    half = Fraction(1, 2)
    two3rds = Fraction(2, 3)
    assert half / -2 == Fraction(-1, 4), 'divide by negative int borked'
    assert half / 2 == Fraction(1, 4), 'divide by int borked'
    assert two3rds / half == Fraction(4, 3), 'divide by Fraction borked'
    assert half / (-1 * half) == Fraction(-1, 1), 'divide by neg Fraction borked'


def tests():
    """ informal unit tests"""
    test_bad_denom_raises()
    test_string()
    test_repr()
    test_equality()
    test_not_equality()
    test_simplify()
    test_add()
    test_subtraction()
    test_multiplication()
    test_division()
    test_inequality()

if '__main__' == __name__:
    tests()
    doctest.testmod()
