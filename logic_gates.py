# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 12:43:15 2015

@author: laurence
"""
from __future__ import print_function
import abc


class LogicGate(object):
    """
    base class for single-out_put logic gates
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, name):
        self.name = name
        self.out_put = None
        self.__in_put = None
        self.update()

    def __repr__(self):
        return '{}(\'{}\').in_put({})'.format(self.__class__.__name__,
                                              self.name, self.__in_put)

    @abc.abstractmethod
    def update(self):
        """update the out_put perhaps in response to changed in_put"""
        raise NotImplementedError(self.__class__.__name__)
        self.out_put = 'never get here'

    @property
    def in_put(self):
        """what is on the single input pin True/False"""
        return self.__in_put

    @in_put.setter
    def in_put(self, new_in_put):
        """change the in_put to `new_input`: either `True` or `False`"""
        self.__in_put = new_in_put
        self.update()


class UnaryGate(LogicGate):
    """single in_put logic gate"""
    def __init__(self, name):
        super(UnaryGate, self).__init__(name)
    # this, like its parent, is abstract...
    # do we need to be explicit about that
    # rather than inheriting abstractness... ?
    # ... pylint sugests we should


class NotGate(UnaryGate):
    """
    single in_put logic-inverting gate
    Init with name then specify input
    all inputs will be co-erced to bool
    """
    def __init__(self, name):
        super(NotGate, self).__init__(name)

    def __repr__(self):
        return 'NotGate(\'{}\', in_put={})'.format(self.name, self.in_put)

    def update(self):
        """update the out_put perhaps in response to changed in_put"""
        if self.in_put is not None:
            self.out_put = not self.in_put


class BinaryGate(LogicGate):
    """2 in_put logic gate"""
    def __init__(self, name):
        super(BinaryGate, self).__init__(name)
        self.update()


class AndGate(BinaryGate):
    """
    dual in_put logic gate are both in_puts truth-y?

    Init with name then specify input
    all inputs will be co-erced to bool
    """
    def __init__(self, name):
        super(AndGate, self).__init__(name)

    def __repr__(self):
        try:
            strout = 'AndGate(\'{}\', in_a={}, in_b={})'.format(
                self.name,
                self.in_put[0],
                self.in_put[1]
                )
        except TypeError as terr:
            if '__getitem__' in str(terr):
                strout = 'AndGate({})'.format(self.name)
        return strout

    def update(self):
        """update the out_put perhaps in response to changed in_put"""
        if self.in_put is not None:
            if None not in self.in_put:
                self.out_put = self.in_put[0] and self.in_put[1]
                if sel


class OrGate(BinaryGate):
    """
    dual in_put logic gate is atleast onof the in_puts truth-y?

    Init with name then specify input
    all inputs will be co-erced to bool"""
    def __init__(self, name):
        super(OrGate, self).__init__(name)

    def __repr__(self):
        try:
            strout = 'AndGate(\'{}\', in_a={}, in_b={})'.format(
                self.name,
                self.in_put[0],
                self.in_put[1]
                )
        except TypeError as terr:
            if '__getitem__' in str(terr):
                strout = 'AndGate({})'.format(self.name)
        return strout

    def update(self):
        """update the out_put perhaps in response to changed in_put"""
        if self.in_put is not None and None not in self.in_put:
            self.out_put = self.in_put[0] or self.in_put[1]


class Connector(object):
    """ connect 2 LogicGates together """
    def __init__(self, to_gate, from_gate):
        self.to_gate = to_gate
        self.from_gate = from_gate
        self.update()

    def update(self):
        self.to_gate.in_put = self.from_gate.out_put


def test_or_gate():
    """is our or gate working?"""
    print('testing OrGate')
    init_none = OrGate('init_none')
    assert init_none.in_put is None, 'shoud get no in_put if not init\'ed '
    assert init_none.out_put is None, ('shoud get no out_put '
                                       'from OrGate with no input')
    init_none.in_put = (True, True)
    assert init_none.in_put == (True, True), 'updating input failed'
    assert init_none.out_put is True, 'updating out_put on changed input failed'
    init_none.in_put = (False, False)
    assert init_none.in_put == (False, False), 'updating input to FF failed'
    assert init_none.out_put is False, 'updating out_put on input FF failed'
    init_none.in_put = (True, False)
    assert init_none.in_put == (True, False), 'updating input to TF failed'
    assert init_none.out_put is True, 'updating out_put on input FF failed'
    init_none.in_put = (False, True)
    assert init_none.in_put == (False, True), 'updating input to FT failed'
    assert init_none.out_put is True, 'updating out_put on input FF failed'


def test_and_gate():
    """is our and gate working?"""
    print('testing AndGate')
    init_none = AndGate('init_none')
    assert init_none.in_put is None, 'shoud get no in_put if not init\'ed '
    assert init_none.out_put is None, ('shoud get no out_put '
                                       'from AndGate with no input')
    init_none.in_put = (True, True)
    assert init_none.in_put == (True, True), 'updating input failed'
    assert init_none.out_put is True, 'updating out_put on changed input failed'
    init_none.in_put = (False, False)
    assert init_none.in_put == (False, False), 'updating input to FF failed'
    assert init_none.out_put is False, 'updating out_put on input FF failed'
    init_none.in_put = (True, False)
    assert init_none.in_put == (True, False), 'updating input to TF failed'
    assert init_none.out_put is False, 'updating out_put on input FF failed'
    init_none.in_put = (False, True)
    assert init_none.in_put == (False, True), 'updating input to FT failed'
    assert init_none.out_put is False, 'updating out_put on input FF failed'


def test_not_gate():
    """test our not gate is working"""
    print('testing NotGate')
    init_none = NotGate('init_none')
    assert init_none.in_put is None, 'shoud get no in_put if not init\'ed '
    assert init_none.out_put is None, ('shoud get no out_put '
                                       'from not gate with no input')
    init_none.in_put = True
    assert init_none.in_put is True, 'updating input failed'
    assert init_none.out_put is False, 'updating out_put on changed input failed'


def run_tests():
    "informal unit tests"""
    print('running tests')
    test_not_gate()
    test_and_gate()
    test_or_gate()

if __name__ == '__main__':
    run_tests()
